# Release Notes for Bitcoin Cash Node version 26.1.1

Bitcoin Cash Node version 26.1.1 is now available from:

  <https://bitcoincashnode.org>

## Overview

This release of Bitcoin Cash Node (BCHN) is a patch release.

...

## Usage recommendations

Users who are running v25.0.0 or v26.x.0 are encouraged to upgrade
to v26.1.1.

## Network changes

TODO

## Added functionality

- Added configuration option `-percentblockmaxsize` which is an alternative to `-blockmaxsize`. `-percentmaxblocksize`
  can be used to configure the node's maximum mined block size as a percentage of the maximum block size for the
  network. So for instance the default on mainnet is `-percentblockmaxsize=25.0` (8 MB). Can be any value from 0.0
  to 100.0.

## Deprecated functionality

TODO.

## Modified functionality

- Added an additional returned data item to the `getmininginfo` RPC: "miningblocksizelimit", which is the block size
  limit used for mining as configured for the node by e.g.: `-blockmaxsize`.
- The Qt GUI RPC console now is fixed to match the HTTP RPC with respect to stateful "config related" commands. Commands
  such as `getmininginfo` and `getexcessiveblock` now match the actual node config (and the HTTP RPC).

## Removed functionality

TODO

## New RPC methods

TODO

## User interface changes

TODO

## Regressions

Bitcoin Cash Node 26.1.1 does not introduce any known regressions as compared
to 26.1.0.

## Limitations

The following are limitations in this release of which users should be aware:

1. CashToken support is low-level at this stage. The wallet application does
   not yet keep track of the user's tokens.
   Tokens are only manageable via RPC commands currently.
   They only persist through the UTXO database and block database at this
   point.
   There are existing RPC commands to list and filter for tokens in the UTXO set.
   RPC raw transaction handling commands have been extended to allow creation
   (and sending) of token transactions.
   Interested users are advised to consult the functional test in
   `test/functional/bchn-rpc-tokens.py` for examples on token transaction
   construction and listing.
   Future releases will aim to extend the RPC API with more convenient
   ways to create and spend tokens, as well as upgrading the wallet storage
   and indexing subsystems to persistently store data about tokens of interest
   to the user. Later we expect to add GUI wallet management of Cash Tokens.

2. Transactions with SIGHASH_UTXO are not covered by DSProofs at present.

3. P2SH-32 is not used by default in the wallet (regular P2SH-20 remains
   the default wherever P2SH is treated).

4. The markup of Double Spend Proof events in the wallet does not survive
   a restart of the wallet, as the information is not persisted to the
   wallet.

## Known Issues

Some issues could not be closed in time for release, but we are tracking all
of them on our GitLab repository.

- The minimum macOS version is 10.14 (Mojave).
  Earlier macOS versions are no longer supported.

- Windows users are recommended not to run multiple instances of bitcoin-qt
  or bitcoind on the same machine if the wallet feature is enabled.
  There is risk of data corruption if instances are configured to use the same
  wallet folder.

- Some users have encountered unit tests failures when running in WSL
  environments (e.g. WSL/Ubuntu).  At this time, WSL is not considered a
  supported environment for the software. This may change in future.
  It has been reported that using WSL2 improves the issue.

- `doc/dependencies.md` needs revision (Issue #65).

- For users running from sources built with BerkeleyDB releases newer than
  the 5.3 which is used in this release, please take into consideration
  the database format compatibility issues described in Issue #34.
  When building from source it is recommended to use BerkeleyDB 5.3 as this
  avoids wallet database incompatibility issues with the official release.

- The `test_bitcoin-qt` test executable fails on Linux Mint 20
  (see Issue #144). This does not otherwise appear to impact the functioning
  of the BCHN software on that platform.

- With a certain combination of build flags that included disabling
  the QR code library, a build failure was observed where an erroneous
  linking against the QR code library (not present) was attempted (Issue #138).

- Possible out-of-memory error when starting bitcoind with high excessiveblocksize
  value (Issue #156)

- A problem was observed on scalenet where nodes would sometimes hang for
  around 10 minutes, accepting RPC connections but not responding to them
  (see #210).

- Startup and shutdown time of nodes on scalenet can be long (see Issue #313).

- Race condition in one of the `p2p_invalid_messages.py` tests (see Issue #409).

- Occasional failure in bchn-txbroadcastinterval.py (see Issue #403).

- wallet_keypool.py test failure when run as part of suite on certain many-core
  platforms (see Issue #380).

- Spurious 'insufficient funds' failure during p2p_stresstest.py benchmark
  (see Issue #377).

- If compiling from source, secp256k1 now no longer works with latest openssl3.x series.
  There are workarounds (see Issue #364).

- Spurious `AssertionError: Mempool sync timed out` in several tests
  (see Issue #357).

- For some platforms, there may be a need to install additional libraries
  in order to build from source (see Issue #431 and discussion in MR 1523).

- More TorV3 static seeds may be needed to get `-onlynet=onion` working
  (see Issue #429).

- Memory usage can be very high if repeatedly doing RPC `getblock` with
  verbose=2 on a hash of known big blocks (see Issue #466).

- Building / testing on current Arch Linux requires some additional package
  installations which are not yet reflected in the documentation.
  (see Issue #477).

- A GUI crash failure was observed when attempting to encrypt a large imported
  wallet (see Issue #490).


---

## Changes since Bitcoin Cash Node 26.1.0

### New documents

TODO

### Removed documents

TODO

### Notable commits grouped by functionality

#### Security or consensus relevant fixes

TODO

#### Interfaces / RPC

TODO

#### Features in internal development: support for UTXO commitments

TODO

### Data directory changes

TODO

#### Performance optimizations

TODO

#### GUI

TODO

#### Code quality

TODO

#### Documentation updates

TODO

#### Build / general

TODO

#### Build / Linux

TODO

#### Build / Windows

TODO

#### Build / MacOSX

TODO

#### Tests / test framework

TODO

#### Benchmarks

TODO

#### Seeds / seeder software

TODO

#### Maintainer tools

TODO

#### Infrastructure

TODO

#### Cleanup

TODO

#### Continuous Integration (GitLab CI)

TODO

#### Backports

TODO
